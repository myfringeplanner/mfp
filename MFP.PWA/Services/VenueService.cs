﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MFP.Models;
using Microsoft.AspNetCore.Components;

namespace MFP.PWA.Services
{
    public class VenueService : IVenueService
    {
        private readonly HttpClient _httpClient;

        public VenueService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<VenueApp>> ReadAll()
        {
            var venues = await _httpClient.GetJsonAsync<VenueApp[]>("api/venues");

            return CleanDemoData(venues);
        }

        // TODO: remove name amendment with real data
        private IEnumerable<VenueApp> CleanDemoData(IEnumerable<VenueApp> venues)
        {
            foreach (var venue in venues)
                venue.Name = venue.Name.Replace("DEMO", "");

            return venues;
        }

        //public async Task<VenueApp> FindById(Guid ID)
        //{
        //    return await _httpClient.GetJsonAsync<VenueApp>($"api/venues/{ID}");
        //}
    }
}
