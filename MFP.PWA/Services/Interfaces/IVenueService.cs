﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.PWA.Services
{
    public interface IVenueService
    {
        Task<IEnumerable<VenueApp>> ReadAll();
        //Task<VenueApp> FindById(Guid ID);
    }
}
