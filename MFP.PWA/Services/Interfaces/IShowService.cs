﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.PWA.Services
{
    public interface IShowService
    {
        Task<IEnumerable<ShowApp>> ReadAll();

    }
}
