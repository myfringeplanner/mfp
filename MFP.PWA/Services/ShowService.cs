﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MFP.Models;
using Microsoft.AspNetCore.Components;

namespace MFP.PWA.Services
{
    public class ShowService : IShowService
    {
        private readonly HttpClient _httpClient;

        public ShowService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<ShowApp>> ReadAll()
        {
            var shows = await _httpClient.GetJsonAsync<ShowApp[]>("api/shows");

            return CleanDemoData(shows);
        }

        // TODO: remove when have real data
        private IEnumerable<ShowApp> CleanDemoData(IEnumerable<ShowApp> shows)
        {
            foreach (var show in shows)
            {
                show.Title = show.Title.Replace("FAKE", "");
                show.Description = show.Description.Replace("DEMO", "");
                show.VenueName = show.VenueName.Replace("DEMO", "");
            };

            return shows;
        }
    }
}
