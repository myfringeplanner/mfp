﻿using System;
using System.Threading.Tasks;
using MFP.Models;
using MFP.PWA.Services;
using Microsoft.AspNetCore.Components;

namespace MFP.PWA.Components
{
    public partial class ShowCard : ComponentBase
    {
        [Parameter] public ShowApp Show { get; set; } = new ShowApp();
        [Parameter] public string Rotation { get; set; } = "";
    }
}
