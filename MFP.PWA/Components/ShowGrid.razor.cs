﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Models;
using MFP.Models.Enums;
using MFP.PWA.Services;
using Microsoft.AspNetCore.Components;

namespace MFP.PWA.Components
{
    public partial class ShowGrid
    {
        [Inject] public IShowService ShowService { get; set; }

        private string SearchText { get; set; } = "";
        private List<string> GenreIds { get; set; } = new List<string> { ((int)Genre.All).ToString() };
        private List<string> VenueIds { get; set; } = new List<string>();
        private DateTime FromDate { get; set; } = DateTime.Parse("2020-08-01");
        private DateTime ToDate { get; set; } = DateTime.Parse("2020-08-31");
        private int OrderById { get; set; } = (int)OrderedBy.TitleAscending;

        public List<ShowApp> Shows { get; set; } = new List<ShowApp>();

        public async Task LoadData(string searchText = "", List<string> genreIds = null, List<string> venueIds = null, DateTime? fromDate = null, DateTime? toDate = null, string orderById = null)
        {
            SearchText = searchText.Trim().ToLower();
            GenreIds = (genreIds != null && genreIds.Count > 0) ? genreIds : new List<string> { ((int)Genre.All).ToString() };
            VenueIds = venueIds;
            FromDate = fromDate.Value;
            ToDate = toDate.Value;
            OrderById = orderById == null ? 0 : int.Parse(orderById);

            Shows = (await ShowService.ReadAll())
                                      .Where(i => (string.IsNullOrEmpty(SearchText) || i.Title.ToLower().Contains(SearchText))
                                             && (VenueIds.Count == 0 || VenueIds.Contains(i.Venue.Code))
                                             && (GenreIds.Contains(((int)Genre.All).ToString()) || GenreIds.Contains(((int)i.Genre).ToString()))
                                             && i.MatchesUserDates(i, FromDate, ToDate))
                                      .ToList();

            OrderShows();

            await InvokeAsync(StateHasChanged);
        }

        private void OrderShows()
        {
            switch (OrderById)
            {
                case (int)OrderedBy.TitleAscending:
                    {
                        Shows = Shows.OrderBy(i => i.Title).ToList();
                        break;
                    }
                case (int)OrderedBy.TitleDescending:
                    {
                        Shows = Shows.OrderByDescending(i => i.Title).ToList();
                        break;
                    }
                case (int)OrderedBy.DateAscending:
                    {
                        Shows = Shows;
                        break;
                    }
                case (int)OrderedBy.DateDescending:
                    {
                        Shows = Shows;
                        break;
                    }
                case (int)OrderedBy.PriceAscending:
                    {
                        Shows = Shows.OrderBy(i => i.PerformancePrice).ThenBy(i => i.Title).ToList();
                        break;

                    }
                case (int)OrderedBy.PriceDescending:
                    {
                        Shows = Shows.OrderByDescending(i => i.PerformancePrice).ThenBy(i => i.Title).ToList();
                        break;

                    }
                case (int)OrderedBy.CreatedAscending:
                    {
                        //Shows = Shows.OrderBy(i => i.Created).ThenBy(i => i.Title).ToList();
                        Shows = Shows;
                        break;
                    }
                case (int)OrderedBy.CreatedDescending:
                    {
                        //Shows = Shows.OrderByDescending(i => i.Created).ThenBy(i => i.Title).ToList();
                        Shows = Shows;
                        break;
                    }
            }
        }
    }
}
