﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Models;
using MFP.Models.Enums;
using MFP.PWA.Components;
using MFP.PWA.Services;
using Microsoft.AspNetCore.Components;

namespace MFP.PWA.Pages.Shows
{
    public partial class Index : ComponentBase
    {
        [Inject] public IShowService ShowService { get; set; }
        [Inject] public IVenueService VenueService { get; set; }

        public string SearchText { get; set; } = "";
        public List<string> GenreIds { get; set; } = new List<string>();
        public List<string> VenueIds { get; set; } = new List<string>();
        public string OrderById { get; set; } = ((int)OrderedBy.TitleAscending).ToString();
        public DateTime FromDate { get; set; } = DateTime.Parse("2020-08-01");
        public DateTime ToDate { get; set; } = DateTime.Parse("2020-08-31");

        public List<ShowApp> FilterShows { get; set; } = new List<ShowApp>();
        public List<SelectItem> FilterGenres { get; set; } = new List<SelectItem>();
        public List<VenueApp> FilterVenues { get; set; } = new List<VenueApp>();
        public List<SelectItem> FilterOrderBy { get; set; } = new List<SelectItem>();

        public IEnumerable<string> SelectedGenres = new string[] { };
        public IEnumerable<string> SelectedVenues = new string[] { };

        public ShowGrid ShowGrid { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await LoadFilters();
            await RefreshShows();
            await base.OnInitializedAsync();
        }

        protected async Task LoadFilters()
        {
            FilterShows = (await ShowService.ReadAll()).ToList();
            SetGenres();
            FilterVenues = (await VenueService.ReadAll()).ToList();
            SetOrderings();
        }

        private void SetGenres()
        {
            FilterGenres = new List<SelectItem>
            {
                new SelectItem
                {
                    ID = ((int)Genre.CabaretVariety).ToString(),
                    Name = EnumHelper.GetDescription(Genre.CabaretVariety)
                },
                new SelectItem
                {
                    ID = ((int)Genre.Childrens).ToString(),
                    Name = EnumHelper.GetDescription(Genre.Childrens)
                },
                new SelectItem
                {
                    ID = ((int)Genre.Comedy).ToString(),
                    Name = EnumHelper.GetDescription(Genre.Comedy)
                },
                new SelectItem
                {
                    ID = ((int)Genre.DancePhysicalCircus).ToString(),
                    Name = EnumHelper.GetDescription(Genre.DancePhysicalCircus)
                },
                new SelectItem
                {
                    ID = ((int)Genre.Events).ToString(),
                    Name = EnumHelper.GetDescription(Genre.Events)
                },
                new SelectItem
                {
                    ID = ((int)Genre.Exhibitions).ToString(),
                    Name = EnumHelper.GetDescription(Genre.Exhibitions)
                },
                new SelectItem
                {
                    ID = ((int)Genre.Music).ToString(),
                    Name = EnumHelper.GetDescription(Genre.Music)
                },
                new SelectItem
                {
                    ID = ((int)Genre.MusicalsOpera).ToString(),
                    Name = EnumHelper.GetDescription(Genre.MusicalsOpera)
                },
                new SelectItem
                {
                    ID = ((int)Genre.SpokenWord).ToString(),
                    Name = EnumHelper.GetDescription(Genre.SpokenWord)
                },
                new SelectItem
                {
                    ID = ((int)Genre.Theatre).ToString(),
                    Name = EnumHelper.GetDescription(Genre.Theatre)
                },
                new SelectItem
                {
                    ID = ((int)Genre.Other).ToString(),
                    Name = EnumHelper.GetDescription(Genre.Other)
                }
            };
        }

        private void SetOrderings()
        {
            FilterOrderBy = new List<SelectItem>
            {
                new SelectItem
                {
                    ID = ((int)OrderedBy.TitleAscending).ToString(),
                    Name = EnumHelper.GetDescription(OrderedBy.TitleAscending)
                },
                new SelectItem
                {
                    ID = ((int)OrderedBy.TitleDescending).ToString(),
                    Name = EnumHelper.GetDescription(OrderedBy.TitleDescending)
                },
                new SelectItem
                {
                    ID = ((int)OrderedBy.DateAscending).ToString(),
                    Name = EnumHelper.GetDescription(OrderedBy.DateAscending)
                },
                new SelectItem
                {
                    ID = ((int)OrderedBy.DateDescending).ToString(),
                    Name = EnumHelper.GetDescription(OrderedBy.DateDescending)
                },
                new SelectItem
                {
                    ID = ((int)OrderedBy.PriceAscending).ToString(),
                    Name = EnumHelper.GetDescription(OrderedBy.PriceAscending)
                },
                new SelectItem
                {
                    ID = ((int)OrderedBy.PriceDescending).ToString(),
                    Name = EnumHelper.GetDescription(OrderedBy.PriceDescending)
                },
                new SelectItem
                {
                    ID = ((int)OrderedBy.CreatedAscending).ToString(),
                    Name = EnumHelper.GetDescription(OrderedBy.CreatedAscending)
                },
                new SelectItem
                {
                    ID = ((int)OrderedBy.CreatedDescending).ToString(),
                    Name = EnumHelper.GetDescription(OrderedBy.CreatedDescending)
                }
            };
        }

        public async Task RefreshShows()
        {
            await ShowGrid.LoadData(SearchText, GenreIds, VenueIds, FromDate, ToDate, OrderById);
        }

        public async Task ClearFilter()
        {

            SearchText = "";
            GenreIds = new List<string>();
            SelectedGenres = new string[] { };
            VenueIds = new List<string>();
            SelectedVenues = new string[] { };
            FromDate = DateTime.Parse("2020-08-01");
            ToDate = DateTime.Parse("2020-08-31");
            OrderById = ((int)OrderedBy.TitleAscending).ToString();

            await RefreshShows();
        }

        public void SearchTextChanged(object value)
        {
            SearchText = (string)value;
        }

        public void GenresChanged(object value)
        {
            GenreIds = new List<string>();

            if (value is IEnumerable<object>)
                foreach (string val in (IEnumerable<object>)value)
                        GenreIds.Add(val);
        }

        public void VenuesChanged(object value)
        {
            VenueIds = new List<string>();

            if (value is IEnumerable<object>)
                foreach (string val in (IEnumerable<object>)value)
                    VenueIds.Add(val);
        }

        public void OrderByChanged(object value)
        {
            OrderById = (string)value;
        }

        public void FromDateChanged(DateTime? value)
        {
            FromDate = value.Value;
        }

        public void ToDateChanged(DateTime? value)
        {
            ToDate = value.Value;
        }
    }
}
