﻿using System;
using System.Threading.Tasks;

namespace MFP.PWA.Shared
{
    public partial class NavBar
    {
        public string ShowsLink { get; set; } = "active-page";
        public string FavouritesLink { get; set; } = "";
        public string PlannerLink { get; set; } = "";

        public void OpenPage(string page)
        {
            switch (page)
            {
                case "shows":
                    {
                        ClearActive();
                        ShowsLink = "active-page";
                        break;
                    }
                case "favourites":
                    {
                        ClearActive();
                        FavouritesLink = "active-page";
                        break;
                    }
                case "planner":
                    {
                        ClearActive();
                        PlannerLink = "active-page";
                        break;
                    }
            };
        }

        private void ClearActive()
        {
            ShowsLink = "";
            FavouritesLink = "";
            PlannerLink = "";
        }
    }
}
