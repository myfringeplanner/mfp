﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MFP.Api.Models
{
    public class VenueApi
    {
        [Key]
        public Guid ID { get; set; }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DisabledDescription { get; set; }
        public string CafeDescription { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool HasBar { get; set; }
        public bool HasCafe { get; set; }
        public string WebAddress { get; set; }
    }
}
