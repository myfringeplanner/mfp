﻿using System;
using System.ComponentModel.DataAnnotations;
using MFP.Models;

namespace MFP.Api.Models
{
    public class PerformanceApi
    {
        [Key]
        public Guid ID { get; set; }

        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public decimal Price { get; set; }
        public decimal Concession { get; set; }
        public decimal ConcessionFamily { get; set; }
        public decimal ConcessionAdditional { get; set; }

        //public virtual ShowApi Show { get; set; }
    }
}
