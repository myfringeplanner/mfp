﻿using System;
using System.ComponentModel.DataAnnotations;
using MFP.Models;

namespace MFP.Api.Models
{
    public class PerformanceSpaceApi
    {
        [Key]
        public Guid ID { get; set; }

        public string Name { get; set; }
        public string Capacity { get; set; }
        public bool WheelchairAccess { get; set; }
        public bool AgeLimited { get; set; }
        public string AgeLimit { get; set; }

        //public virtual VenueApi Venue { get; set; }
    }
}
