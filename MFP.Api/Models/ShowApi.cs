﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MFP.Models;

namespace MFP.Api.Models
{
    public class ShowApi
    {
        [Key]
        public Guid ID { get; set; }

        public int Year { get; set; }
        public Status Status { get; set; }
        public Genre Genre { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Warnings { get; set; }
        public int AgeCategory { get; set; }
        public string Website { get; set; }
        public string Url { get; set; }
        public string Twitter { get; set; }

        public string ImageOriginal { get; set; }
        public string ImageThumbnail { get; set; }

        public bool TwoForOneDiscount { get; set; }
        public bool GroupDiscount { get; set; }
        public bool FriendsDiscount { get; set; }
        public bool PassportDiscount { get; set; }
        public bool SchoolsDiscount { get; set; }

        public DateTime LastUpdated { get; set; }

        public VenueApi Venue { get; set; }
        public PerformanceSpaceApi PerformanceSpace { get; set; }

        public virtual ICollection<PerformanceApi> Performances { get; set; }
        //public virtual ICollection<FavouriteApiModel> Favourites { get; set; }

        public string VenueName { get; set; }
        public string PerformancePrice { get; set; }
        public string PerformanceTimes { get; set; }
        public string PerformanceDates { get; set; }
    }
}
