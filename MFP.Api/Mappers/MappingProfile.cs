﻿using System;
using AutoMapper;
using MFP.Api.Models;
using MFP.Models;

namespace MFP.Api.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Model to ApiModel
            CreateMap<Show, ShowApi>().ForMember(i => i.VenueName, j => j.MapFrom(k => k.Venue.Name));
            CreateMap<Performance, PerformanceApi>();
            CreateMap<Venue, VenueApi>();
            CreateMap<PerformanceSpace, PerformanceSpaceApi>();
        }
    }
}
