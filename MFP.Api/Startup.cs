using AutoMapper;
using MFP.Api.Controllers;
using MFP.Api.Repositories;
using MFP.Api.Services;
using MFP.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MFP.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MFPDbContext>(options => options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddAutoMapper(typeof(Startup));

            services.AddScoped<IVenueRepository, VenueRepository>();
            services.AddScoped<IVenueService, VenueService>();
            services.AddScoped<IBaseService<Venue>, VenueService>();

            services.AddScoped<IPerformanceSpaceRepository, PerformanceSpaceRepository>();
            services.AddScoped<IPerformanceSpaceService, PerformanceSpaceService>();
            services.AddScoped<IBaseService<PerformanceSpace>, PerformanceSpaceService>();

            services.AddScoped<IShowRepository, ShowRepository>();
            services.AddScoped<IShowService, ShowService>();
            services.AddScoped<IBaseService<Show>, ShowService>();

            services.AddScoped<IPerformanceRepository, PerformanceRepository>();
            services.AddScoped<IPerformanceService, PerformanceService>();
            services.AddScoped<IBaseService<Performance>, PerformanceService>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IBaseService<User>, UserService>();
            services.AddScoped<IEntityService<User>, UserService>();

            services.AddScoped<IFavouriteRepository, FavouriteRepository>();
            services.AddScoped<IFavouriteService, FavouriteService>();
            services.AddScoped<IBaseService<Favourite>, FavouriteService>();
            services.AddScoped<IEntityService<Favourite>, FavouriteService>();

            services.AddScoped<IBookingRepository, BookingRepository>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<IBaseService<Booking>, BookingService>();
            services.AddScoped<IEntityService<Booking>, BookingService>();

            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();

            services.AddCors(i => i.AddPolicy("Policy", builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); }));

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseCors("Policy");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
