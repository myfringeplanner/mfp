﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Api.Repositories;
using MFP.Models;

namespace MFP.Api.Services
{
    public class ShowService : IShowService
    {
        private readonly IRepositoryWrapper _repositoryWrapper;

        public ShowService(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<IEnumerable<Show>> ReadAllWithLinkedEntities()
        {
            return await _repositoryWrapper.ShowRepository.ReadAllWithLinkedEntities();
        }

        public async Task<Show> FindByIDWithLinkedEntities(Guid ID)
        {
            return await _repositoryWrapper.ShowRepository.FindByIDWithLinkedEntities(ID);
        }

        public async Task<IEnumerable<Show>> ReadAll()
        {
            return await _repositoryWrapper.ShowRepository.ReadAll();
        }

        public async Task<Show> FindByID(Guid ID)
        {
            return await _repositoryWrapper.ShowRepository.FindByID(ID);
        }

        //public async Task<IEnumerable<Show>> ReadFavouriteShowsByUser(Guid ID)
        //{
        //    return await _repositoryWrapper.ShowRepository.ReadFavouriteShowsByUser(ID);
        //}

        //public async Task<IEnumerable<Show>> Recommended(Guid ID)
        //{
        //    return await _repositoryWrapper.ShowRepository.RecommendedShows(ID);
        //}

        //public async Task<IEnumerable<Show>> Popular()
        //{
        //    return await _repositoryWrapper.ShowRepository.PopularShows();
        //}
    }
}
