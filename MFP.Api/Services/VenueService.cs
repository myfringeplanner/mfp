﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Api.Repositories;
using MFP.Models;

namespace MFP.Api.Services
{
    public class VenueService : IVenueService
    {
        private readonly IRepositoryWrapper _repositoryWrapper;

        public VenueService(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<IEnumerable<Venue>> ReadAll()
        {
            return await _repositoryWrapper.VenueRepository.ReadAll();
        }

        public async Task<Venue> FindByID(Guid ID)
        {
            return await _repositoryWrapper.VenueRepository.FindByID(ID);
        }
    }
}
