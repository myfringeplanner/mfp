﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Api.Repositories;
using MFP.Models;

namespace MFP.Api.Services
{
    public class PerformanceService : IPerformanceService
    {
        private readonly IRepositoryWrapper _repositoryWrapper;

        public PerformanceService(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }


        // INHERITANCES

        public async Task<IEnumerable<Performance>> ReadAll()
        {
            return await _repositoryWrapper.PerformanceRepository.ReadAll();
        }

        public async Task<Performance> FindByID(Guid ID)
        {
            return await _repositoryWrapper.PerformanceRepository.FindByID(ID);
        }

        //public async Task<IEnumerable<Performance>> ReadAll(DateTime start, DateTime end)
        //{
        //    return await _repositoryWrapper.PerformanceRepository.ReadAll(start, end);
        //}

        //public async Task<IEnumerable<Performance>> ReadAllByShow(Guid ID)
        //{
        //    return await _repositoryWrapper.PerformanceRepository.ReadAllByShow(ID);
        //}

        //public async Task<IEnumerable<Performance>> ReadBookingPerformancesByUser(Guid ID)
        //{
        //    return await _repositoryWrapper.PerformanceRepository.ReadBookingPerformancesByUser(ID);
        //}
    }
}
