﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Api.Repositories;
using MFP.Models;

namespace MFP.Api.Services
{
    public class BookingService : IBookingService
    {
        private readonly IRepositoryWrapper _repositoryWrapper;

        public BookingService(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }


        // INHERITANCES

        public async Task<IEnumerable<Booking>> ReadAll()
        {
            return await _repositoryWrapper.BookingRepository.ReadAll();
        }

        public async Task<Booking> FindByID(Guid ID)
        {
            return await _repositoryWrapper.BookingRepository.FindByID(ID);
        }

        public async Task<Booking> New(Booking entity)
        {
            var booking = await _repositoryWrapper.BookingRepository.New(entity);
            _repositoryWrapper.Save();

            return booking;
        }

        public void Delete(Booking entity)
        {
            _repositoryWrapper.BookingRepository.Delete(entity);
            _repositoryWrapper.Save();
        }

        public void DeleteAll(IEnumerable<Booking> entities)
        {
            _repositoryWrapper.BookingRepository.DeleteAll(entities);
            _repositoryWrapper.Save();
        }


        // UNIMPLEMENTED INHERITANCES

        public Task<Booking> Edit(Booking entity, Booking update)
        {
            throw new NotImplementedException();
        }
    }
}
