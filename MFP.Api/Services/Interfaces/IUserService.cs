﻿using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Services
{
    public interface IUserService : IBaseService<User>, IEntityService<User>
    {
        Task<User> FindByEmail(string email);
    }
}
