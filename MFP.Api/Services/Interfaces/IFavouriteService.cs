﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Api.Repositories;
using MFP.Models;

namespace MFP.Api.Services
{
    public interface IFavouriteService : IBaseService<Favourite>, IEntityService<Favourite>
    {
        
    }
}
