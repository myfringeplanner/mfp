﻿using System;
using System.Threading.Tasks;

namespace MFP.Api.Services
{
    public interface IFringeService
    {
        Task UpdateFringeData(dynamic json);
    }
}
