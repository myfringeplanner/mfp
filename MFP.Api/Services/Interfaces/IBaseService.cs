﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MFP.Api.Services
{
    public interface IBaseService<TEntity>
        where TEntity : class
    {
        Task<IEnumerable<TEntity>> ReadAll();
        Task<TEntity> FindByID(Guid ID);

        //Task<TEntity> New(TEntity entity);
        //Task<TEntity> Edit(TEntity entity, TEntity update);

        //void Delete(Guid ID);
        //void DeleteAll(IEnumerable<TEntity> entities);
    }
}
