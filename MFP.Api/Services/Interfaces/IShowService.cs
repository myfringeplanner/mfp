﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Services
{
    public interface IShowService : IBaseService<Show>
    {

        Task<IEnumerable<Show>> ReadAllWithLinkedEntities();
        Task<Show> FindByIDWithLinkedEntities(Guid ID);

        //Task<IEnumerable<Show>> ReadFavouriteShowsByUser(Guid ID);
        //Task<IEnumerable<Show>> Recommended(Guid ID);
        //Task<IEnumerable<Show>> Popular();
    }
}
