﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MFP.Api.Services
{
    public interface IEntityService<TEntity>
        where TEntity : class
    {
        Task<TEntity> New(TEntity entity);
        Task<TEntity> Edit(TEntity entity, TEntity update);

        void Delete(TEntity entity);
        void DeleteAll(IEnumerable<TEntity> entities);
    }
}
