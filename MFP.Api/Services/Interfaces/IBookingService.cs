﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Services
{
    public interface IBookingService : IBaseService<Booking>, IEntityService<Booking>
    {
        
    }
}
