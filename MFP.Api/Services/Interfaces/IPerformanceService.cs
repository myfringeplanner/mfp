﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Services
{
    public interface IPerformanceService : IBaseService<Performance>
    {
        //Task<IEnumerable<Performance>> ReadAll(DateTime start, DateTime end);
        //Task<IEnumerable<Performance>> ReadAllByShow(Guid ID);
        //Task<IEnumerable<Performance>> ReadBookingPerformancesByUser(Guid ID);
    }
}
