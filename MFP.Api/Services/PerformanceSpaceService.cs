﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Api.Repositories;
using MFP.Models;

namespace MFP.Api.Services
{
    public class PerformanceSpaceService : IPerformanceSpaceService
    {
        private readonly IRepositoryWrapper _repositoryWrapper;

        public PerformanceSpaceService(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<IEnumerable<PerformanceSpace>> ReadAll()
        {
            return await _repositoryWrapper.PerformanceSpaceRepository.ReadAll();
        }

        public async Task<PerformanceSpace> FindByID(Guid ID)
        {
            return await _repositoryWrapper.PerformanceSpaceRepository.FindByID(ID);
        }
    }
}
