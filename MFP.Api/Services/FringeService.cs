﻿using System;
using System.Threading.Tasks;
using MFP.Api.Repositories;
using MFP.Models;

namespace MFP.Api.Services
{
    public class FringeService : IFringeService
    {
        private readonly RepositoryWrapper _repositoryWrapper;

        public FringeService(RepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }


        public async Task UpdateFringeData(dynamic listings)
        {
            foreach (var listing in listings)
            {
                // TODO: account for year
                if (listing.year == 2020)
                {
                    var venue = await _repositoryWrapper.VenueRepository.CreateOrUpdate(listing);
                    var performanceSpace = await _repositoryWrapper.PerformanceSpaceRepository.CreateOrUpdate(listing, venue);
                    var show = await _repositoryWrapper.ShowRepository.CreateOrUpdate(listing, venue, performanceSpace);
                    await _repositoryWrapper.PerformanceRepository.CreateOrUpdate(listing, show);
                    _repositoryWrapper.Save();
                }
            }
        }
    }
}
