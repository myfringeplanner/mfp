﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Api.Repositories;
using MFP.Models;

namespace MFP.Api.Services
{
    public class UserService : IUserService
    {
        private readonly IRepositoryWrapper _repositoryWrapper;

        public UserService(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        // INHERITANCES

        public async Task<IEnumerable<User>> ReadAll()
        {
            return await _repositoryWrapper.UserRepository.ReadAll();
        }

        public async Task<User> FindByID(Guid ID)
        {
            return await _repositoryWrapper.UserRepository.FindByID(ID);
        }

        public async Task<User> FindByEmail(string email)
        {
            return await _repositoryWrapper.UserRepository.FindByEmail(email);
        }

        public async Task<User> New(User entity)
        {
            var user = await _repositoryWrapper.UserRepository.New(entity);
            _repositoryWrapper.Save();

            return user;
        }

        public async Task<User> Edit(User entity, User update)
        {
            var user = await _repositoryWrapper.UserRepository.Edit(entity, update);
            _repositoryWrapper.Save();

            return user;
        }

        public void Delete(User entity)
        {
            _repositoryWrapper.UserRepository.Delete(entity);
            _repositoryWrapper.Save();
        }

        public void DeleteAll(IEnumerable<User> entities)
        {
            _repositoryWrapper.UserRepository.DeleteAll(entities);
            _repositoryWrapper.Save();
        }
    }
}
