﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Api.Repositories;
using MFP.Models;

namespace MFP.Api.Services
{
    public class FavouriteService : IFavouriteService
    {
        private readonly IRepositoryWrapper _repositoryWrapper;

        public FavouriteService(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<IEnumerable<Favourite>> ReadAll()
        {
            return await _repositoryWrapper.FavouriteRepository.ReadAll();
        }

        public async Task<Favourite> FindByID(Guid ID)
        {
            return await _repositoryWrapper.FavouriteRepository.FindByID(ID);
        }

        public async Task<Favourite> New(Favourite entity)
        {
            var favourite = await _repositoryWrapper.FavouriteRepository.New(entity);
            _repositoryWrapper.Save();

            return favourite;
        }

        public void Delete(Favourite entity)
        {
            _repositoryWrapper.FavouriteRepository.Delete(entity);
            _repositoryWrapper.Save();
        }

        public void DeleteAll(IEnumerable<Favourite> entities)
        {
            _repositoryWrapper.FavouriteRepository.DeleteAll(entities);
            _repositoryWrapper.Save();
        }


        // UNIMPLEMENTED INHERITANCES

        public Task<Favourite> Edit(Favourite entity, Favourite update)
        {
            throw new NotImplementedException();
        }
    }
}
