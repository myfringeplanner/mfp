﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using MFP.Api.Migrations.Configurations;
using MFP.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MFP.Api
{
    public class MFPDbContext : DbContext
    {
        public MFPDbContext(DbContextOptions<MFPDbContext> options) : base(options)
        {

        }

        public DbSet<Show> Shows { get; set; }
        public DbSet<Performance> Performances { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<PerformanceSpace> PerformanceSpaces { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Favourite> Favourites { get; set; }
        public DbSet<Booking> Bookings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new VenueConfiguration());
            modelBuilder.ApplyConfiguration(new PerformanceSpaceConfiguration());
            modelBuilder.ApplyConfiguration(new ShowConfiguration());
            modelBuilder.ApplyConfiguration(new PerformanceConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new FavouriteConfiguration());
            modelBuilder.ApplyConfiguration(new BookingConfiguration());

            modelBuilder.Entity<User>().HasData(
                new
                {
                    ID = Guid.NewGuid(),
                    FirstName = "Admin",
                    LastName = "Master",
                    Email = "deeylye@gmail.com",
                    Password = "password123",
                    Created = DateTime.UtcNow
                }
            );

            SeedDatabase(modelBuilder);
        }

        private void SeedDatabase(ModelBuilder modelBuilder)
        {
            var venues = new List<Venue>();
            var performanceSpaces = new List<PerformanceSpace>();

            var path = "https://api.edinburghfestivalcity.com/events?festival=demofringe";
            var key = "&key=SBCFydR3Qwac3UJo";
            var signature = "&signature=8497616766ba3965051a0da1b411c3fddf08d98e";
            var size = "&size=100";
            int[] pages = { 0, 100, 200, 300 };

            foreach (var page in pages)
            {
                var url = string.Format("{0}{1}{2}{3}&from={4}", path, key, signature, size, page.ToString());
                HttpResponseMessage response = new HttpClient().GetAsync(url).Result;

                byte[] buf = response.Content.ReadAsByteArrayAsync().Result;
                string content = Encoding.UTF8.GetString(buf);
                dynamic listings = JsonConvert.DeserializeObject(content);

                foreach (dynamic listing in listings)
                {
                    Venue venue = GetVenue(listing.venue, venues);
                    AddVenueToSeed(venue, venues, modelBuilder);

                    PerformanceSpace performanceSpace = GetPerformanceSpace(listing.performance_space, performanceSpaces, venue);
                    AddPerformanceSpaceToSeed(performanceSpace, performanceSpaces, modelBuilder);

                    Show show = GetShow(listing, venue, performanceSpace);
                    AddShowToSeed(show, modelBuilder);

                    List<Performance> newPerformances = GetPerformances(listing.performances, show);
                    AddPerformancesToSeed(newPerformances, modelBuilder);
                };
            };
        }

        private Venue GetVenue(dynamic listing, List<Venue> venues)
        {
            var existingVenue = venues.FirstOrDefault(i => i.Code == listing.code.ToString());

            if (existingVenue == null)
            {
                var newVenue = new Venue
                {
                    ID = Guid.NewGuid(),
                    Code = listing.code.ToString(),
                    Name = listing.name?.ToString() ?? "-",
                    Description = listing?.description.ToString() ?? "-",
                    DisabledDescription = listing.disabled_description?.ToString() ?? "-",
                    CafeDescription = listing.cafe_description?.ToString() ?? "-",
                    Address = listing.address?.ToString() ?? "-",
                    Postcode = listing.post_code?.ToString() ?? "-",
                    Phone = listing.phone?.ToString() ?? "-",
                    Email = listing.email?.ToString() ?? "-",
                    WebAddress = listing.web_address?.ToString() ?? "-",
                    HasBar = listing.has_bar.ToString() == "True" ? true : false,
                    HasCafe = listing.has_cafe.ToString() == "True" ? true : false
                };

                return newVenue;
            }
            else
            {
                return existingVenue;
            };
        }

        private void AddVenueToSeed(Venue venue, List<Venue> venues, ModelBuilder modelBuilder)
        {
            if (venues.FirstOrDefault(i => i.Code == venue.Code) == null)
            {
                venues.Add(venue);
                modelBuilder.Entity<Venue>().HasData(venue);
            };
        }

        private PerformanceSpace GetPerformanceSpace(dynamic listing, List<PerformanceSpace> performanceSpaces, Venue venue)
        {
            var existingPerformanceSpace = performanceSpaces.FirstOrDefault(i => i.Name == listing.name.ToString());

            if (existingPerformanceSpace == null)
            {
                var newPerformanceSpace = new PerformanceSpace
                {
                    ID = Guid.NewGuid(),
                    VenueID = venue.ID,
                    Name = listing.name?.ToString() ?? "-",
                    Capacity = listing.capacity?.ToString() ?? "-",
                    WheelchairAccess = listing.wheelchair_access.ToString() == "True" ? true : false,
                    AgeLimited = listing.age_limited.ToString() == "True" ? true : false,
                    AgeLimit = listing.age_limit?.ToString() ?? ""
                };

                return newPerformanceSpace;
            }
            else
            {
                return existingPerformanceSpace;
            };
        }

        private void AddPerformanceSpaceToSeed(PerformanceSpace performanceSpace, List<PerformanceSpace> performanceSpaces, ModelBuilder modelBuilder)
        {
            if (performanceSpaces.FirstOrDefault(i => i.Name == performanceSpace.Name) == null)
            {
                performanceSpaces.Add(performanceSpace);
                modelBuilder.Entity<PerformanceSpace>().HasData(performanceSpace);
            };
        }

        private Show GetShow(dynamic listing, Venue venue, PerformanceSpace performanceSpace)
        {
            var show = new Show
            {
                ID = Guid.NewGuid(),
                VenueID = venue.ID,
                PerformanceSpaceID = performanceSpace.ID,
                Year = int.Parse(listing.year.ToString()),
                Status = EnumHelper.GetEnumFromDescription<Status>(listing.status.ToString()),
                Genre = EnumHelper.GetEnumFromDescription<Genre>(listing.genre.ToString()),
                Title = listing.title?.ToString() ?? "-",
                Description = listing.description?.ToString() ?? "-",
                Warnings = listing.warnings?.ToString() ?? "-",
                AgeCategory = listing.age_category == null ? 0 : int.Parse(listing.age_category?.ToString().Replace("+", "")),
                Website = listing.website?.ToString() ?? "-",
                Url = listing.url?.ToString() ?? "-",
                Twitter = listing.twitter?.ToString() ?? "-",
                TwoForOneDiscount = listing.discounts.two_for_one.ToString() == "True" ? true : false,
                GroupDiscount = listing.discounts.group.ToString() == "True" ? true : false,
                FriendsDiscount = listing.discounts.friends.ToString() == "True" ? true : false,
                PassportDiscount = listing.discounts.passport.ToString() == "True" ? true : false,
                SchoolsDiscount = listing.discounts.schools.ToString() == "True" ? true : false,
                LastUpdated = DateTime.Parse(listing.updated.ToString())
            };

            foreach (var child in listing.images)
            {
                show.ImageOriginal = string.Format("https:{0}", child.First.versions.original.url.ToString()) ?? "logo.png";
                show.ImageThumbnail = string.Format("https:{0}", child.First.versions["thumb-100"].url.ToString()) ?? "logo.png";
            };

            return show;
        }

        private void AddShowToSeed(Show show, ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Show>().HasData(show);
        }

        private List<Performance> GetPerformances(dynamic listing, Show show)
        {
            var performances = new List<Performance>();

            foreach (var list in listing)
            {
                performances.Add(new Performance
                {
                    ID = Guid.NewGuid(),
                    ShowID = show.ID,
                    Title = list.title?.ToString() ?? "-",
                    Start = DateTime.Parse(list.start.ToString()),
                    End = DateTime.Parse(list.end.ToString()),
                    Price = decimal.Parse(list.price.ToString()),
                    Concession = list.concession.ToString() != "False" ? decimal.Parse(list.concession.ToString()) : decimal.Parse(list.price.ToString()),
                    ConcessionFamily = list.concession_family >= 0 ? list.concession_family : list.price,
                    ConcessionAdditional = list.concession_additional.ToString() == "False" ? decimal.Parse(list.price.ToString()) : list.concession_additional
                });
            };

            return performances;
        }

        private void AddPerformancesToSeed(List<Performance> performances, ModelBuilder modelBuilder)
        {
            foreach (var performance in performances)
            {
                modelBuilder.Entity<Performance>().HasData(performance);
            };
        }
    }
}
