﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Models;
using Microsoft.EntityFrameworkCore;

namespace MFP.Api.Repositories
{
    public class FavouriteRepository : BaseRepository<Favourite>, IFavouriteRepository
    {
        public FavouriteRepository(MFPDbContext mfpDbContext)
            : base(mfpDbContext)
        {
            
        }

        public async Task<Favourite> New(Favourite entity)
        {
            var result = await _mfpDbContext.Set<Favourite>().AddAsync(entity);

            return result.Entity;
        }

        public void Delete(Favourite entity)
        {
            _mfpDbContext.Set<Favourite>().Remove(entity);
        }

        public void DeleteAll(IEnumerable<Favourite> entities)
        {
            _mfpDbContext.Set<Favourite>().RemoveRange(entities);
        }

        // UNIMPLEMENTED INHERITANCES

        public Task<Favourite> Edit(Favourite entity, Favourite update)
        {
            throw new NotImplementedException();
        }
    }
}
