﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MFP.Api.Repositories
{
    public abstract class EntityRepository<TEntity> : IEntityRepository<TEntity>
        where TEntity : class
    {
        private readonly MFPDbContext _mfpDbContext;

        public EntityRepository(MFPDbContext mfpDbContext)
        {
            _mfpDbContext = mfpDbContext;
        }

        public async Task<TEntity> New(TEntity entity)
        {
            var result = await _mfpDbContext.Set<TEntity>().AddAsync(entity);

            return result.Entity;
        }

        public Task<TEntity> Edit(TEntity entity, TEntity update)
        {
            throw new NotImplementedException();
        }

        public void Delete(TEntity entity)
        {
            _mfpDbContext.Set<TEntity>().Remove(entity);
        }

        public void DeleteAll(IEnumerable<TEntity> entities)
        {
            _mfpDbContext.Set<TEntity>().RemoveRange(entities);
        }
    }
}
