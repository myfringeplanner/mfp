﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Models;
using Microsoft.EntityFrameworkCore;

namespace MFP.Api.Repositories
{
    public class PerformanceRepository : BaseRepository<Performance>, IPerformanceRepository
    {
        public PerformanceRepository(MFPDbContext mfpDbContext)
            : base(mfpDbContext)
        {

        }

        public async Task<IEnumerable<Performance>> CreateOrUpdate(dynamic listing, Show show)
        {
            var existingPerformances = show.Performances;
            var listedPerformances = new List<Performance>();

            foreach (var list in listing.performances)
            {
                var performance = ConvertJson(list, show);
                listedPerformances.Add(performance);
                CreateOrUpdate(performance, existingPerformances);
            };

            foreach (var performance in existingPerformances)
                CheckForDelete(performance, listedPerformances);

            return await _mfpDbContext.Performances.Where(i => i.ShowID == show.ID).ToListAsync();
        }

        
        private Performance ConvertJson(dynamic list, Show show)
        {
            return new Performance
            {
                ID = Guid.NewGuid(),
                ShowID = show.ID,
                Title = list.title?.ToString() ?? "-",
                Start = DateTime.Parse(list.start.ToString()),
                End = DateTime.Parse(list.end.ToString()),
                Price = decimal.Parse(list.price.ToString()),
                Concession = list.concession.ToString() != "False" ? decimal.Parse(list.concession.ToString()) : decimal.Parse(list.price.ToString()),
                ConcessionFamily = list.concession_family >= 0 ? list.concession_family : list.price,
                ConcessionAdditional = list.concession_additional.ToString() == "False" ? decimal.Parse(list.price.ToString()) : list.concession_additional
            };
        }

        private async void CreateOrUpdate(Performance performance, ICollection<Performance> existingPerformances)
        {
            var check = existingPerformances.FirstOrDefault(i => i.Start == performance.Start && i.End == performance.End);

            if (check == null)
            {
                await _mfpDbContext.Performances.AddAsync(performance);
            }
            else
            {
                check.Title = performance.Title;
                check.Price = performance.Price;
                check.Concession = performance.Concession;
                check.ConcessionFamily = performance.ConcessionFamily;
                check.ConcessionAdditional = performance.ConcessionAdditional;
            };
        }

        private void CheckForDelete(Performance performance, ICollection<Performance> listedPerformances)
        {
            var check = listedPerformances.FirstOrDefault(i => i.Start == performance.Start && i.End == performance.End);

            if (check == null)
                _mfpDbContext.Performances.Remove(performance);
        }

        //public async Task<IEnumerable<Performance>> ReadAll(DateTime start, DateTime end)
        //{
        //    var performances = await _mfpDbContext.Performances.ToListAsync();

        //    return performances.Where(i => i.Start >= start && i.Start < end).ToList();
        //}

        //public async Task<IEnumerable<Performance>> ReadAllByShow(Guid ID)
        //{
        //    var show = await _mfpDbContext.Shows.Where(i => i.ID == ID).Include(i => i.Performances).FirstOrDefaultAsync();

        //    return show.Performances;
        //}

        //public async Task<IEnumerable<Performance>> ReadBookingPerformancesByUser(Guid ID)
        //{
        //    return await _mfpDbContext.Bookings.Where(i => i.UserID == ID).Select(i => i.Performance).OrderBy(i => i.Start).ToListAsync();
        //}
    }
}
