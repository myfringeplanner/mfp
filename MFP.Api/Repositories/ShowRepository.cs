﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Models;
using Microsoft.EntityFrameworkCore;

namespace MFP.Api.Repositories
{
    public class ShowRepository : BaseRepository<Show>, IShowRepository
    {
        public ShowRepository(MFPDbContext mfpDbContext)
            : base(mfpDbContext)
        {

        }

        public override async Task<IEnumerable<Show>> ReadAllWithLinkedEntities()
        {
            return await _mfpDbContext.Set<Show>().Include(i => i.Venue)
                                                  .Include(i => i.PerformanceSpace)
                                                  .Include(i => i.Performances)
                                                  .OrderBy(i => i.Title)
                                                  .ToListAsync();
        }

        public override async Task<Show> FindByIDWithLinkedEntities(Guid ID)
        {
            return await _mfpDbContext.Set<Show>().Include(i => i.Venue)
                                                  .Include(i => i.PerformanceSpace)
                                                  .Include(i => i.Performances)
                                                  .FirstOrDefaultAsync(i => i.ID == ID);
        }

        public async Task<Show> CreateOrUpdate(dynamic listing, Venue venue, PerformanceSpace performanceSpace)
        {
            Show show = await FindByUrl(listing.url);

            if (show == null)
                return await Create(listing, venue, performanceSpace);
            else if (DateTime.Parse(listing.updated) > DateTime.UtcNow.AddDays(-1))
                return await Update(listing, show, venue, performanceSpace);
            else
                return show;
        }

        public async Task<Show> FindByUrl(string url)
        {
            return await _mfpDbContext.Shows.FirstOrDefaultAsync(i => i.Url == url);
        }

        public async Task<Show> Create(dynamic listing, Venue venue, PerformanceSpace performanceSpace)
        {
            var result = await _mfpDbContext.AddAsync(new Show
            {
                ID = Guid.NewGuid(),
                VenueID = venue.ID,
                PerformanceSpaceID = performanceSpace.ID,
                Year = int.Parse(listing.year.ToString()),
                Status = EnumHelper.GetEnumFromDescription<Status>(listing.status.ToString()),
                Genre = EnumHelper.GetEnumFromDescription<Genre>(listing.genre.ToString()),
                Title = listing.title?.ToString() ?? "-",
                Description = listing.description?.ToString() ?? "-",
                Warnings = listing.warnings?.ToString() ?? "-",
                AgeCategory = listing.age_category == null ? 0 : int.Parse(listing.age_category?.ToString().Replace("+", "")),
                Website = listing.website?.ToString() ?? "-",
                Url = listing.url?.ToString() ?? "-",
                Twitter = listing.twitter?.ToString() ?? "-",
                TwoForOneDiscount = listing.discounts.two_for_one.ToString() == "True" ? true : false,
                GroupDiscount = listing.discounts.group.ToString() == "True" ? true : false,
                FriendsDiscount = listing.discounts.friends.ToString() == "True" ? true : false,
                PassportDiscount = listing.discounts.passport.ToString() == "True" ? true : false,
                SchoolsDiscount = listing.discounts.schools.ToString() == "True" ? true : false,
                LastUpdated = DateTime.Parse(listing.updated.ToString())
            });

            return result.Entity;
        }

        public async Task<Show> Update(dynamic listing, Show show, Venue venue, PerformanceSpace performanceSpace)
        {
            var result = await _mfpDbContext.Shows.FirstOrDefaultAsync(i => i.ID == show.ID);

            if (result != null)
            {
                result.VenueID = venue.ID;
                result.PerformanceSpaceID = performanceSpace.ID;
                result.Year = int.Parse(listing.year.ToString());
                result.Status = EnumHelper.GetEnumFromDescription<Status>(listing.status.ToString());
                result.Genre = EnumHelper.GetEnumFromDescription<Genre>(listing.genre.ToString());
                result.Title = listing.title?.ToString() ?? "-";
                result.Description = listing.description?.ToString() ?? "-";
                result.Warnings = listing.warnings?.ToString() ?? "-";
                result.AgeCategory = listing.age_category == null ? 0 : int.Parse(listing.age_category?.ToString().Replace("+", ""));
                result.Website = listing.website?.ToString() ?? "-";
                result.Url = listing.url?.ToString() ?? "-";
                result.Twitter = listing.twitter?.ToString() ?? "-";
                result.TwoForOneDiscount = listing.discounts.two_for_one.ToString() == "True" ? true : false;
                result.GroupDiscount = listing.discounts.group.ToString() == "True" ? true : false;
                result.FriendsDiscount = listing.discounts.friends.ToString() == "True" ? true : false;
                result.PassportDiscount = listing.discounts.passport.ToString() == "True" ? true : false;
                result.SchoolsDiscount = listing.discounts.schools.ToString() == "True" ? true : false;
                result.LastUpdated = DateTime.Parse(listing.updated.ToString());

                return result;
            };

            return null;
        }

        //public async Task<IEnumerable<Show>> ReadFavouriteShowsByUser(Guid ID)
        //{
        //    return await _mfpDbContext.Favourites.Where(i => i.UserID == ID).Select(i => i.Show).OrderBy(i => i.Title).ToListAsync();
        //}

        //public async Task<IEnumerable<Show>> RecommendedShows(Guid ID)
        //{
        //    var user = await _mfpDbContext.Users.FirstOrDefaultAsync(i => i.ID == ID);
        //    var userShows = new List<Show>();
        //    var recommendedShows = new List<Show>();

        //    foreach (var favourite in user.Favourites)
        //        userShows.Add(favourite.Show);

        //    foreach (var show in userShows)
        //    {
        //        var otherUsers = _mfpDbContext.Favourites.Where(i => i.ShowID == show.ID).Select(i => i.User);

        //        foreach (var u in otherUsers)
        //            foreach (var fave in u.Favourites)
        //                if (fave.ShowID != show.ID && !recommendedShows.Contains(fave.Show))
        //                    recommendedShows.Add(fave.Show);
        //    }

        //    return recommendedShows.OrderBy(i => i.Title);
        //}

        //public async Task<IEnumerable<Show>> PopularShows()
        //{
        //    var popularShows = await _mfpDbContext.Favourites.GroupBy(i => i.ShowID).OrderByDescending(i => i.Count()).SelectMany(i => i).ToListAsync();

        //    return popularShows.Select(i => i.Show).Distinct();
        //}
    }
}
