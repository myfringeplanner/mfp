﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MFP.Api.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : class
    {
        protected MFPDbContext _mfpDbContext { get; set; }

        public BaseRepository(MFPDbContext MFPDbContext)
        {
            _mfpDbContext = MFPDbContext;
        }

        public async Task<IEnumerable<TEntity>> ReadAll()
        {
            return await _mfpDbContext.Set<TEntity>().ToListAsync();
        }

        public virtual Task<IEnumerable<TEntity>> ReadAllWithLinkedEntities()
        {
            throw new NotImplementedException();
        }

        public ValueTask<TEntity> FindByID(Guid ID)
        {
            return _mfpDbContext.Set<TEntity>().FindAsync(ID);
        }

        public virtual Task<TEntity> FindByIDWithLinkedEntities(Guid ID)
        {
            throw new NotImplementedException();
        }

        //public async Task<TEntity> Read(Expression<Func<TEntity, bool>> expression)
        //{
        //    return await _mfpDbContext.Set<TEntity>().FirstOrDefaultAsync(expression);
        //}

        //public IEnumerable<TEntity> ReadAll(Expression<Func<TEntity, bool>> expression)
        //{
        //    return _mfpDbContext.Set<TEntity>().Where(expression);
        //}
    }
}
