﻿using System;
using System.Threading.Tasks;
using MFP.Models;
using Microsoft.EntityFrameworkCore;

namespace MFP.Api.Repositories
{
    public class PerformanceSpaceRepository : BaseRepository<PerformanceSpace>, IPerformanceSpaceRepository
    {
        public PerformanceSpaceRepository(MFPDbContext mfpDbContext)
            : base(mfpDbContext)
        {

        }

        public async Task<PerformanceSpace> CreateOrUpdate(dynamic listing, Venue venue)
        {
            PerformanceSpace performanceSpace = await FindByName(listing.performance_space.name);

            if (performanceSpace == null)
                return await Create(listing.performance_space, venue);
            else if (DateTimeOffset.FromUnixTimeSeconds(listing.update_times.venue) > DateTime.UtcNow.AddDays(-1))
                return await Update(listing.performance_space, performanceSpace, venue);
            else
                return performanceSpace;
        }

        public async Task<PerformanceSpace> FindByName(string name)
        {
            return await _mfpDbContext.PerformanceSpaces.FirstOrDefaultAsync(i => i.Name == name);
        }

        public async Task<PerformanceSpace> Create(dynamic listing, Venue venue)
        {
            var result = await _mfpDbContext.PerformanceSpaces.AddAsync(new PerformanceSpace
            {
                ID = Guid.NewGuid(),
                VenueID = venue.ID,
                Name = listing.name?.ToString() ?? "-",
                Capacity = listing.capacity?.ToString() ?? "-",
                WheelchairAccess = listing.wheelchair_access.ToString() == "True" ? true : false,
                AgeLimited = listing.age_limited.ToString() == "True" ? true : false,
                AgeLimit = listing.age_limit?.ToString() ?? ""
            });

            return result.Entity;
        }

        public async Task<PerformanceSpace> Update(dynamic listing, PerformanceSpace performanceSpace, Venue venue)
        {
            var result = await _mfpDbContext.PerformanceSpaces.FirstOrDefaultAsync(i => i.ID == performanceSpace.ID);

            if (result != null)
            {
                result.VenueID = venue.ID;
                result.Name = listing.name?.ToString() ?? "-";
                result.Capacity = listing.capacity?.ToString() ?? "-";
                result.WheelchairAccess = listing.wheelchair_access.ToString() == "True" ? true : false;
                result.AgeLimited = listing.age_limited.ToString() == "True" ? true : false;
                result.AgeLimit = listing.age_limit?.ToString() ?? "";

                return result;
            };

            return null;
        }
    }
}
