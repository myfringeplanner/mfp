﻿using System;
using System.Threading.Tasks;
using MFP.Models;
using Microsoft.EntityFrameworkCore;

namespace MFP.Api.Repositories
{
    public class VenueRepository : BaseRepository<Venue>, IVenueRepository
    {
        public VenueRepository(MFPDbContext mfpDbContext)
            : base(mfpDbContext)
        {

        }

        public async Task<Venue> CreateOrUpdate(dynamic listing)
        {
            Venue venue = await FindByCode(listing.venue.code);

            if (venue == null)
                return await Create(listing.venue);
            else if (DateTimeOffset.FromUnixTimeSeconds(listing.update_times.venue) > DateTime.UtcNow.AddDays(-1))
                return await Update(listing.venue, venue);
            else
                return venue;
        }

        public async Task<Venue> FindByCode(string code)
        {
            return await _mfpDbContext.Venues.FirstOrDefaultAsync(i => i.Code == code);
        }

        public async Task<Venue> Create(dynamic listing)
        {
            var result = await _mfpDbContext.Venues.AddAsync(new Venue
            {
                ID = Guid.NewGuid(),
                Code = listing.code.ToString(),
                Name = listing.name?.ToString() ?? "-",
                Description = listing?.description.ToString() ?? "-",
                DisabledDescription = listing.disabled_description?.ToString() ?? "-",
                CafeDescription = listing.cafe_description?.ToString() ?? "-",
                Address = listing.address?.ToString() ?? "-",
                Postcode = listing.post_code?.ToString() ?? "-",
                Phone = listing.phone?.ToString() ?? "-",
                Email = listing.email?.ToString() ?? "-",
                WebAddress = listing.web_address?.ToString() ?? "-",
                HasBar = listing.has_bar.ToString() == "True" ? true : false,
                HasCafe = listing.has_cafe.ToString() == "True" ? true : false
            });

            return result.Entity;
        }

        public async Task<Venue> Update(dynamic listing, Venue venue)
        {
            var result = await _mfpDbContext.Venues.FirstOrDefaultAsync(i => i.ID == venue.ID);

            if (result != null)
            {
                result.Name = listing.name?.ToString() ?? "-";
                result.Description = listing?.description.ToString() ?? "-";
                result.DisabledDescription = listing.disabled_description?.ToString() ?? "-";
                result.CafeDescription = listing.cafe_description?.ToString() ?? "-";
                result.Address = listing.address?.ToString() ?? "-";
                result.Postcode = listing.post_code?.ToString() ?? "-";
                result.Phone = listing.phone?.ToString() ?? "-";
                result.Email = listing.email?.ToString() ?? "-";
                result.WebAddress = listing.web_address?.ToString() ?? "-";
                result.HasBar = listing.has_bar.ToString() == "True" ? true : false;
                result.HasCafe = listing.has_cafe.ToString() == "True" ? true : false;

                return result;
            };

            return null;
        }
    }
}
