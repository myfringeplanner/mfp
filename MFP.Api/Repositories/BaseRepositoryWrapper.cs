﻿using System;
namespace MFP.Api.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private MFPDbContext _mfpDbContext;
        private IVenueRepository _venueRepository;
        private IPerformanceSpaceRepository _performanceSpaceRepository;
        private IShowRepository _showRepository;
        private IPerformanceRepository _performanceRepository;
        private IUserRepository _userRepository;
        private IFavouriteRepository _favouriteRepository;
        private IBookingRepository _bookingRepository;

        public IVenueRepository VenueRepository
        {
            get
            {
                if (_venueRepository == null)
                    _venueRepository = new VenueRepository(_mfpDbContext);

                return _venueRepository;
            }
        }

        public IPerformanceSpaceRepository PerformanceSpaceRepository
        {
            get
            {
                if (_performanceSpaceRepository == null)
                    _performanceSpaceRepository = new PerformanceSpaceRepository(_mfpDbContext);

                return _performanceSpaceRepository;
            }
        }

        public IShowRepository ShowRepository
        {
            get
            {
                if (_showRepository == null)
                    _showRepository = new ShowRepository(_mfpDbContext);

                return _showRepository;
            }
        }

        public IPerformanceRepository PerformanceRepository
        {
            get
            {
                if (_performanceRepository == null)
                    _performanceRepository = new PerformanceRepository(_mfpDbContext);

                return _performanceRepository;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_mfpDbContext);

                return _userRepository;
            }
        }

        public IFavouriteRepository FavouriteRepository
        {
            get
            {
                if (_favouriteRepository == null)
                    _favouriteRepository = new FavouriteRepository(_mfpDbContext);

                return _favouriteRepository;
            }
        }

        public IBookingRepository BookingRepository
        {
            get
            {
                if (_bookingRepository == null)
                    _bookingRepository = new BookingRepository(_mfpDbContext);

                return _bookingRepository;
            }
        }

        public RepositoryWrapper(MFPDbContext mfpDbContext)
        {
            _mfpDbContext = mfpDbContext;
        }

        public void Save()
        {
            _mfpDbContext.SaveChangesAsync();
        }
    }
}
