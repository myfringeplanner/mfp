﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Repositories
{
    public interface IUserRepository : IBaseRepository<User>, IEntityRepository<User>
    {
        Task<User> FindByEmail(string email);
    }               
}
