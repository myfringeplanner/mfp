﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Repositories
{
    public interface IBookingRepository : IBaseRepository<Booking>, IEntityRepository<Booking>
    {
        
    }
}
