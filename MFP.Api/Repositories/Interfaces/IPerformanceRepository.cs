﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Repositories
{
    public interface IPerformanceRepository : IBaseRepository<Performance>
    {
        Task<IEnumerable<Performance>> CreateOrUpdate(dynamic json, Show show);
        //Task<IEnumerable<Performance>> ReadAll(DateTime start, DateTime end);
        //Task<IEnumerable<Performance>> ReadAllByShow(Guid ID);
        //Task<IEnumerable<Performance>> ReadBookingPerformancesByUser(Guid ID);
    }
}
