﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Repositories
{
    public interface IVenueRepository : IBaseRepository<Venue>
    {
        Task<Venue> CreateOrUpdate(dynamic json);
        Task<Venue> FindByCode(string code);
        Task<Venue> Create(dynamic json);
        Task<Venue> Update(dynamic json, Venue venue);
    }
}