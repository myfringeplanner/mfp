﻿namespace MFP.Api.Repositories
{
    public interface IRepositoryWrapper
    {
        IVenueRepository VenueRepository { get; }
        IPerformanceSpaceRepository PerformanceSpaceRepository { get; }
        IShowRepository ShowRepository { get; }
        IPerformanceRepository PerformanceRepository { get; }
        IUserRepository UserRepository { get; }
        IFavouriteRepository FavouriteRepository { get; }
        IBookingRepository BookingRepository { get; }

        void Save();
    }
}
