﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Repositories
{
    public interface IShowRepository : IBaseRepository<Show>
    {
        Task<Show> CreateOrUpdate(dynamic json, Venue venue, PerformanceSpace performanceSpace);
        Task<Show> FindByUrl(string url);
        Task<Show> Create(dynamic json, Venue venue, PerformanceSpace performanceSpace);
        Task<Show> Update(dynamic json, Show show, Venue venue, PerformanceSpace performanceSpace);
        //Task<IEnumerable<Show>> ReadFavouriteShowsByUser(Guid ID);
        //Task<IEnumerable<Show>> RecommendedShows(Guid ID);
        //Task<IEnumerable<Show>> PopularShows();
    }
}
