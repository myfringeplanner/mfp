﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MFP.Api.Repositories
{
    public interface IBaseRepository<TEntity>
        where TEntity : class
    {
        Task<IEnumerable<TEntity>> ReadAll();
        Task<IEnumerable<TEntity>> ReadAllWithLinkedEntities();
        ValueTask<TEntity> FindByID(Guid ID);
        Task<TEntity> FindByIDWithLinkedEntities(Guid ID);
        //IEnumerable<TEntity> ReadAll(Expression<Func<TEntity, bool>> expression);
        //Task<TEntity> Read(Expression<Func<TEntity, bool>> expression);
    }
}
