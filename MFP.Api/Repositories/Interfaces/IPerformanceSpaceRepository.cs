﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MFP.Models;

namespace MFP.Api.Repositories
{
    public interface IPerformanceSpaceRepository : IBaseRepository<PerformanceSpace>
    {
        Task<PerformanceSpace> CreateOrUpdate(dynamic json, Venue venue);
        Task<PerformanceSpace> FindByName(string name);
        Task<PerformanceSpace> Create(dynamic json, Venue venue);
        Task<PerformanceSpace> Update(dynamic json, PerformanceSpace performanceSpace, Venue venue);
    }
}
