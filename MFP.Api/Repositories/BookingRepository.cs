﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Models;
using Microsoft.EntityFrameworkCore;

namespace MFP.Api.Repositories
{
    public class BookingRepository : BaseRepository<Booking>, IBookingRepository
    {
        public BookingRepository(MFPDbContext mfpDbContext)
            : base(mfpDbContext)
        {
            
        }

        public async Task<Booking> New(Booking entity)
        {
            var result = await _mfpDbContext.Set<Booking>().AddAsync(entity);

            return result.Entity;
        }

        public void Delete(Booking entity)
        {
            _mfpDbContext.Set<Booking>().Remove(entity);
        }

        public void DeleteAll(IEnumerable<Booking> entities)
        {
            _mfpDbContext.Set<Booking>().RemoveRange(entities);
        }

        // UNIMPLEMENTED INHERITANCES

        public Task<Booking> Edit(Booking entity, Booking update)
        {
            throw new NotImplementedException();
        }
    }
}
