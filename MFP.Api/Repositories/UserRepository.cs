﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Models;
using Microsoft.EntityFrameworkCore;

namespace MFP.Api.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(MFPDbContext mfpDbContext)
            : base(mfpDbContext)
        {

        }

        public async Task<User> FindByEmail(string email)
        {
            return await _mfpDbContext.Users.FirstOrDefaultAsync(i => i.Email.Trim().ToLower() == email.Trim().ToLower());
        }

        public async Task<User> New(User entity)
        {
            var result = await _mfpDbContext.Set<User>().AddAsync(entity);

            return result.Entity;
        }

        public async Task<User> Edit(User user, User update)
        {
            var result = await _mfpDbContext.Users.FirstOrDefaultAsync(i => i.ID == user.ID);

            if (result != null)
            {
                result.FirstName = update.FirstName;
                result.LastName = update.LastName;
                result.Email = update.Email;
                result.Password = update.Password;

                return result;
            };

            return null;
        }

        public void Delete(User entity)
        {
            _mfpDbContext.Set<User>().Remove(entity);
        }

        public void DeleteAll(IEnumerable<User> entities)
        {
            _mfpDbContext.Set<User>().RemoveRange(entities);
        }
    }
}
