﻿using MFP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MFP.Api.Migrations.Configurations
{
    public class PerformanceSpaceConfiguration : IEntityTypeConfiguration<PerformanceSpace>
    {
        public void Configure (EntityTypeBuilder<PerformanceSpace> builder)
        {
            builder.HasKey(i => i.ID);

            builder.HasOne(i => i.Venue).WithMany(i => i.PerformanceSpaces).HasForeignKey(i => i.VenueID);

            builder.ToTable("PerformanceSpaces");
        }
    }
}
