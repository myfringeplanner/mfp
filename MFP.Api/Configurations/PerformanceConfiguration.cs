﻿using MFP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class PerformanceConfiguration : IEntityTypeConfiguration<Performance>
{
    public void Configure(EntityTypeBuilder<Performance> builder)
    {
        builder.HasKey(i => i.ID);

        builder.HasOne(i => i.Show).WithMany(i => i.Performances).HasForeignKey(i => i.ShowID);

        builder.HasMany(i => i.Bookings).WithOne(i => i.Performance).HasForeignKey(i => i.PerformanceID);

        builder.ToTable("Performances");
    }
}