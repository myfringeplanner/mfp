﻿using MFP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class UserConfiguration : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.HasKey(i => i.ID);

        builder.HasMany(i => i.Favourites).WithOne(i => i.User).HasForeignKey(i => i.UserID);
        builder.HasMany(i => i.Bookings).WithOne(i => i.User).HasForeignKey(i => i.UserID);

        builder.ToTable("Users");
    }
}