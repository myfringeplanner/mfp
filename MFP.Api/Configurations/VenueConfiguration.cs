﻿using MFP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MFP.Api.Migrations.Configurations
{
    public class VenueConfiguration : IEntityTypeConfiguration<Venue>
    {
        public void Configure(EntityTypeBuilder<Venue> builder)
        {
            builder.HasKey(i => i.ID);

            builder.HasMany(i => i.PerformanceSpaces).WithOne(i => i.Venue).HasForeignKey(i => i.VenueID);

            builder.ToTable("Venues");
        }
    }
}
