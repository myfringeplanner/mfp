﻿using MFP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class BookingConfiguration : IEntityTypeConfiguration<Booking>
{
    public void Configure(EntityTypeBuilder<Booking> builder)
    {
        builder.HasKey(i => i.ID);

        builder.HasOne(i => i.Performance).WithMany(i => i.Bookings).HasForeignKey(i => i.PerformanceID);
        builder.HasOne(i => i.User).WithMany(i => i.Bookings).HasForeignKey(i => i.UserID);

        builder.ToTable("Bookings");
    }
}