﻿using MFP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MFP.Api.Migrations.Configurations
{
    public class ShowConfiguration : IEntityTypeConfiguration<Show>
    {
        public void Configure(EntityTypeBuilder<Show> builder)
        {
            builder.HasKey(i => i.ID);

            builder.HasOne(i => i.Venue).WithMany(i => i.Shows).HasForeignKey(i => i.VenueID);
            builder.HasOne(i => i.PerformanceSpace).WithMany(i => i.Shows).HasForeignKey(i => i.PerformanceSpaceID);

            builder.HasMany(i => i.Performances).WithOne(i => i.Show).HasForeignKey(i => i.ShowID);
            builder.HasMany(i => i.Favourites).WithOne(i => i.Show).HasForeignKey(i => i.ShowID);

            builder.ToTable("Shows");
        }
    }
}
