﻿using MFP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class FavouriteConfiguration : IEntityTypeConfiguration<Favourite>
{
    public void Configure(EntityTypeBuilder<Favourite> builder)
    {
        builder.HasKey(i => i.ID);

        builder.HasOne(i => i.Show).WithMany(i => i.Favourites).HasForeignKey(i => i.ShowID);
        builder.HasOne(i => i.User).WithMany(i => i.Favourites).HasForeignKey(i => i.UserID);

        builder.ToTable("Favourites");
    }
}