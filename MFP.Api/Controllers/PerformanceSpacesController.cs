﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Api.Services;
using MFP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MFP.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PerformanceSpacesController : ControllerBase
    {
        private readonly IPerformanceSpaceService _service;

        public PerformanceSpacesController(IPerformanceSpaceService service)
        {
            _service = service;
        }

        // GET: api/performancespaces
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Performance>>> Read()
        {
            try
            {
                var performanceSpaces = await _service.ReadAll();
                performanceSpaces = performanceSpaces.OrderBy(i => i.Name);

                return Ok(performanceSpaces);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/performancespaces/8
        [HttpGet("{id}")]
        public async Task<ActionResult<PerformanceSpace>> Read(Guid ID)
        {
            try
            {
                var performanceSpace = await _service.FindByID(ID);

                if (performanceSpace == null)
                    return NotFound();

                return Ok(performanceSpace);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
