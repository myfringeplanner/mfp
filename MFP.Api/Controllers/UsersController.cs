﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Api.Services;
using MFP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MFP.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _service;

        public UsersController(IUserService service)
        {
            _service = service;
        }

        // GET: api/users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> Read()
        {
            try
            {
                var users = await _service.ReadAll();
                users = users.OrderBy(i => i.FirstName).ThenBy(i => i.LastName).ThenBy(i => i.Email);

                return Ok(users);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Read(Guid ID)
        {
            try
            {
                var user = await _service.FindByID(ID);

                if (user == null)
                    return NotFound();

                return user;
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }

        // POST api/users
        [HttpPost]
        public async Task<ActionResult<User>> New(User user)
        {
            try
            {
                if (user == null)
                    return BadRequest();

                var check = await _service.FindByEmail(user.Email);

                if (check != null)
                {
                    ModelState.AddModelError("email", "Email already in use");
                    return BadRequest(ModelState);
                };

                var newUser = await _service.New(user);

                return CreatedAtAction(nameof(Read), new { id = newUser.ID }, newUser);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }

        // PUT api/users/5
        [HttpPut("{id}")]
        public async Task<ActionResult<User>> Edit(Guid ID, User user)
        {
            try
            {
                if (ID == user.ID)
                    return BadRequest("User ID mismatch");

                var existingUser = await _service.FindByID(ID);

                if (existingUser == null)
                    return NotFound($"User with ID {ID} not found");

                return await _service.Edit(existingUser, user);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error updating data");
            }
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid ID)
        {
            try
            {
                var existingUser = await _service.FindByID(ID);

                if (existingUser == null)
                    return NotFound($"User with ID {ID} not found");

                _service.Delete(existingUser);

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting data");
            }
        }

        // DELETE api/users
        [HttpDelete]
        public async Task<ActionResult> Delete(IEnumerable<User> entities)
        {
            try
            {
                foreach (var entity in entities)
                    if (await _service.FindByID(entity.ID) == null)
                        return NotFound($"User with ID {entity.ID} not found");

                _service.DeleteAll(entities);

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting data");
            }
        }
    }
}
