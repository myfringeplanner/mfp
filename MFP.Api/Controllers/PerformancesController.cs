﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Api.Services;
using MFP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MFP.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PerformancesController : ControllerBase
    {
        private readonly IPerformanceService _service;

        public PerformancesController(IPerformanceService service)
        {
            _service = service;
        }

        // GET: api/performances
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Performance>>> Read()
        {
            try
            {
                var performances = await _service.ReadAll();
                performances = performances.OrderBy(i => i.Start);

                return Ok(performances);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/performances/8
        [HttpGet("{id}")]
        public async Task<ActionResult<Performance>> Read(Guid ID)
        {
            try
            {
                var performance = await _service.FindByID(ID);

                if (performance == null)
                    return NotFound();

                return Ok(performance);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/performances
        //[HttpGet]
        //public async Task<ActionResult> Read(DateTime start, DateTime end)
        //{
        //    try
        //    {
        //        // TODO: remove startDate and endDate
        //        var startDate = DateTime.Parse("2020-01-01");
        //        var endDate = DateTime.Parse("2020-12-31");
        //        return Ok(await _service.ReadAll(startDate, endDate));
        //    }
        //    catch
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError);
        //    }
        //}

        // GET: api/bookings
        //[HttpGet]
        //public async Task<IEnumerable<Performance>> ReadBookingPerformancesByUser(Guid ID)
        //{
        //    return await _service.ReadBookingPerformancesByUser(ID);
        //}

        //public async Task<ActionResult> ShowPerformances(Guid ID)
        //{
        //    try
        //    {
        //        return Ok(await _service.ReadAllFromShow(ID));
        //    }
        //    catch
        //    {
        //        return NotFound();
        //    }
        //}
    }
}
