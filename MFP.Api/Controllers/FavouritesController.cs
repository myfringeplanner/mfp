﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Api.Services;
using MFP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MFP.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavouritesController : ControllerBase
    {
        private readonly IFavouriteService _service;

        public FavouritesController(IFavouriteService service)
        {
            _service = service;
        }

        // GET: api/favourites
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Favourite>>> Read()
        {
            try
            {
                var favourites = await _service.ReadAll();
                favourites = favourites.OrderBy(i => i.Show.Title);

                return Ok(favourites);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/favourites/8
        [HttpGet("{id}")]
        public async Task<ActionResult<Favourite>> Read(Guid ID)
        {
            try
            {
                var favourite = await _service.FindByID(ID);

                if (favourite == null)
                    return NotFound();

                return Ok(favourite);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: api/favourites
        [HttpPost]
        public async Task<ActionResult<Favourite>> New(Favourite favourite)
        {
            try
            {
                if (favourite == null)
                    return BadRequest();

                var newFavourite = await _service.New(favourite);

                return CreatedAtAction(nameof(Read), new { id = newFavourite.ID }, newFavourite);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/favourites
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid ID)
        {
            try
            {
                var favourite = await _service.FindByID(ID);

                if (favourite == null)
                    return NotFound($"Favourite with ID {ID} not found");

                _service.Delete(favourite);

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/favourites
        [HttpDelete]
        public async Task<ActionResult> Delete(IEnumerable<Favourite> entities)
        {
            try
            {
                foreach (var entity in entities)
                    if (await _service.FindByID(entity.ID) == null)
                        return NotFound($"User with ID {entity.ID} not found");

                _service.DeleteAll(entities);

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting data");
            }
        }
    }
}
