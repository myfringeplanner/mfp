﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MFP.Api.Models;
using MFP.Api.Services;
using MFP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MFP.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShowsController : ControllerBase
    {
        private readonly IShowService _service;
        private readonly IMapper _mapper;

        public ShowsController(IShowService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/shows
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Show>>> Read()
        //{
        //    try
        //    {
        //        var shows = await _service.ReadAll();
        //        shows = shows.OrderBy(i => i.Title);

        //        return Ok(shows);
        //    }
        //    catch
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError);
        //    }
        //}

        
        // GET: api/shows
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShowApi>>> Read()
        {
            try
            {
                var shows = await _service.ReadAllWithLinkedEntities();
                var showApis = _mapper.Map<IEnumerable<Show>, IEnumerable<ShowApi>>(shows);

                foreach (var show in showApis)
                {
                    SetPerformancePrice(show);
                    SetPerformanceTimes(show);
                    SetPerformanceDates(show);
                }

                return Ok(showApis);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private void SetPerformancePrice(ShowApi show)
        {
            var price = show.Performances.Last().Price;

            if (price % 1 == 0)
                show.PerformancePrice = price.ToString("0");
            else
                show.PerformancePrice = price.ToString("F");
        }

        private void SetPerformanceTimes(ShowApi show)
        {
            var times = new List<string>();

            foreach (var performance in show.Performances)
            {
                var time = "";
                var duration = performance.End - performance.Start;
                var hours = duration.Hours;
                var minutes = duration.Minutes;

                if (hours == 0)
                    time = $"{performance.Start:HH:mm} ({minutes}min)";
                else if (minutes == 0)
                    time = $"{performance.Start:HH:mm} ({hours}h)";
                else
                    time = $"{performance.Start:HH:mm} ({hours}h {minutes}min)";

                if (!times.Contains(time))
                    times.Add(time);
            };

            show.PerformanceTimes = String.Join(", ", times.ToArray());
        }

        private void SetPerformanceDates(ShowApi show)
        {
            var dates = new List<int>();

            foreach (var performance in show.Performances)
            {
                int date;

                if (performance.Start < performance.Start.Date.AddDays(0.25))
                    date = performance.Start.Day - 1;
                else
                    date = performance.Start.Day;

                if (!dates.Contains(date))
                    dates.Add(date);
            };

            dates = dates.OrderBy(i => i).ToList();

            var alldates = "Aug ";

            for (int i = 0; i < dates.Count; i++)
            {
                if (i == 0 && dates.Count == 1)
                    alldates += dates[i];
                else if (i == 0)
                    alldates += dates[i] + "-";
                else if (i == dates.Count - 1)
                    alldates += dates[i].ToString();
                else if (dates[i] != dates[i - 1] + 1 && dates[i] == dates[i + 1] - 1)
                    alldates += dates[i].ToString() + "-";
                else if ((dates[i] != dates[i - 1] + 1 && dates[i] != dates[i + 1] - 1) || (dates[i] == dates[i - 1] + 1 && dates[i] != dates[i + 1] - 1))
                    alldates += dates[i].ToString() + ", ";
            };

            show.PerformanceDates = alldates;
        }

        // GET: api/shows/8
        //[HttpGet("{id}")]
        //public async Task<ActionResult<Show>> Read(Guid ID)
        //{
        //    try
        //    {
        //        var show = await _service.FindByID(ID);

        //        if (show == null)
        //            return NotFound();

        //        return Ok(show);
        //    }
        //    catch
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError);
        //    }
        //}

        // GET: api/shows/8
        [HttpGet("{id}")]
        public async Task<ActionResult<Show>> Read(Guid ID)
        {
            try
            {
                var show = await _service.FindByIDWithLinkedEntities(ID);

                if (show == null)
                    return NotFound();

                return Ok(show);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/shows
        //[HttpGet]
        //public async Task<ActionResult> ReadFavouriteShowsByUser(Guid ID)
        //{
        //    try
        //    {
        //        var shows = await _service.ReadFavouriteShowsByUser(ID);

        //        if (shows == null)
        //            return NotFound();

        //        return Ok(shows);
        //    }
        //    catch
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError);
        //    }
        //}

        // GET: api/shows/recommended
        //public async Task<ActionResult<IEnumerable<Show>>> Recommended(Guid ID)
        //{
        //    try
        //    {
        //        if (ID == null)
        //            return new List<Show>();

        //        return Ok(await _service.Recommended(ID));
        //    }
        //    catch
        //    {
        //        return new List<Show>();
        //    }
        //}

        // GET: api/popular
        //public async Task<ActionResult<IEnumerable<Show>>> Popular()
        //{
        //    try
        //    {
        //        return Ok(await _service.Popular());
        //    }
        //    catch
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError);
        //    }
        //}
    }
}
