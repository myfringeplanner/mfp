﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Api.Services;
using MFP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MFP.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VenuesController : ControllerBase
    {
        private readonly IVenueService _service;

        public VenuesController(IVenueService service)
        {
            _service = service;
        }

        // GET: api/venues
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Venue>>> Read()
        {
            try
            {
                var venues = await _service.ReadAll();
                venues = venues.OrderBy(i => i.Name);

                return Ok(venues);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/venues/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Venue>> Read(Guid ID)
        {
            try
            {
                var venue = await _service.FindByID(ID);

                if (venue == null)
                    return NotFound();

                return Ok(venue);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
