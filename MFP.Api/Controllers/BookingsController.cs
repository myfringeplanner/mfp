﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFP.Api.Services;
using MFP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MFP.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingsController : ControllerBase
    {
        private readonly IBookingService _service;

        public BookingsController(IBookingService service)
        {
            _service = service;
        }

        // GET: api/bookings
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Booking>>> Read()
        {
            try
            {
                var bookings = await _service.ReadAll();
                bookings = bookings.OrderBy(i => i.Performance.Start);

                return Ok(bookings);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/bookings/8
        [HttpGet("{id}")]
        public async Task<ActionResult<Booking>> Read(Guid ID)
        {
            try
            {
                var booking = await _service.FindByID(ID);

                if (booking == null)
                    return NotFound();

                return Ok(booking);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: api/bookings
        [HttpPost]
        public async Task<ActionResult<Booking>> New(Booking booking)
        {
            try
            {
                if (booking == null)
                    return BadRequest();

                var newBooking = await _service.New(booking);

                return CreatedAtAction(nameof(Read), new { id = newBooking.ID }, newBooking);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/bookings
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid ID)
        {
            try
            {
                var booking = await _service.FindByID(ID);

                if (booking == null)
                    return NotFound($"Booking with ID {ID} not found");

                _service.Delete(booking);

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/bookings
        [HttpDelete]
        public async Task<ActionResult> Delete(IEnumerable<Booking> entities)
        {
            try
            {
                foreach (var entity in entities)
                    if (await _service.FindByID(entity.ID) == null)
                        return NotFound($"User with ID {entity.ID} not found");

                _service.DeleteAll(entities);

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting data");
            }
        }
    }
}
