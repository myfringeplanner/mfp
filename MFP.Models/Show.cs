﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MFP.Models
{
    public class Show
    {
        [Key]
        public Guid ID { get; set; }
        public Guid VenueID { get; set; }
        public Guid PerformanceSpaceID { get; set; }

        public int Year { get; set; }
        public Status Status { get; set; }
        public Genre Genre { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Warnings { get; set; }
        public int AgeCategory { get; set; }
        public string Website { get; set; }
        public string Url { get; set; }
        public string Twitter { get; set; }

        public string ImageOriginal { get; set; }
        public string ImageThumbnail { get; set; }

        public bool TwoForOneDiscount { get; set; }
        public bool GroupDiscount { get; set; }
        public bool FriendsDiscount { get; set; }
        public bool PassportDiscount { get; set; }
        public bool SchoolsDiscount { get; set; }

        public DateTime LastUpdated { get; set; }

        public virtual Venue Venue { get; set; }
        public virtual PerformanceSpace PerformanceSpace { get; set; }

        public virtual ICollection<Performance> Performances { get; set; }
        public virtual ICollection<Favourite> Favourites { get; set; }
    }
}
