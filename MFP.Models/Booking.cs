﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MFP.Models
{
    public class Booking
    {
        [Key]
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
        public Guid PerformanceID { get; set; }

        public virtual User User { get; set; }
        public virtual Performance Performance { get; set; }
    }
}
