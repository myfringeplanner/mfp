﻿using System;
using System.ComponentModel;

namespace MFP.Models.Enums
{
    public enum OrderedBy
    {
        [Description("Title: A-Z")] TitleAscending,
        [Description("Title: Z-A")] TitleDescending,
        [Description("Date: Aug-Sept")] DateAscending,
        [Description("Date: Sept-Aug")] DateDescending,
        [Description("Price: £-£££")] PriceAscending,
        [Description("Price: £££-£")] PriceDescending,
        [Description("Created: Oldest First")] CreatedAscending,
        [Description("Created: Newest First")] CreatedDescending,
    }
}
