﻿using System.ComponentModel;

namespace MFP.Models
{
    public enum Status
    {
        [Description("active")]
        Active,
        [Description("cancelled")]
        Cancelled,
        [Description("deleted")]
        Deleted
    }
}
