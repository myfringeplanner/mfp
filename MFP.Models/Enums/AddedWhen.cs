﻿using System;
using System.ComponentModel;

namespace MFP.Models.Enums
{
    public enum AddedWhen
    {
        [Description("Added At Any Time")] All,
        [Description("Added Inside 1 Week Ago")] OneWeekOld,
        [Description("Added Inside 2 Weeks Ago")] TwoWeeksOld,
        [Description("Added Inside 1 Month Ago")] OneMonthOld
    }
}
