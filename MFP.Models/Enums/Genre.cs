﻿using System.ComponentModel;

namespace MFP.Models
{
    public enum Genre
    {
        [Description("Cabaret and Variety")]
        CabaretVariety,
        [Description("Children's Shows")]
        Childrens,
        [Description("Comedy")]
        Comedy,
        [Description("Dance Physical Theatre and Circus")]
        DancePhysicalCircus,
        [Description("Events")]
        Events,
        [Description("Exhibitions")]
        Exhibitions,
        [Description("Music")]
        Music,
        [Description("Musicals and Opera")]
        MusicalsOpera,
        [Description("Spoken Word")]
        SpokenWord,
        [Description("Theatre")]
        Theatre,
        [Description("Other")]
        Other,
        [Description("All Genres")]
        All
    }
}
