﻿using System;

namespace MFP.Models
{
    public class EnumItem
    {
        public string Description { get; set; }
        public string Value { get; set; }

        public EnumItem()
        {
            Description = string.Empty;
            Value = string.Empty;
        }

        public EnumItem(string Description, string Value)
        {
            this.Description = Description ?? string.Empty;
            this.Value = Value ?? string.Empty;
        }
    }
}