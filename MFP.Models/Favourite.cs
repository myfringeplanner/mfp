﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MFP.Models
{
    public class Favourite
    {
        [Key]
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
        public Guid ShowID { get; set; }

        public virtual User User { get; set; }
        public virtual Show Show { get; set; }
    }
}
