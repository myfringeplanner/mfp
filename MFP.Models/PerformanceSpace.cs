﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MFP.Models
{
    public class PerformanceSpace
    {
        [Key]
        public Guid ID { get; set; }
        public Guid VenueID { get; set; }

        public string Name { get; set; }
        public string Capacity { get; set; }
        public bool WheelchairAccess { get; set; }
        public bool AgeLimited { get; set; }
        public string AgeLimit { get; set; }

        public virtual Venue Venue { get; set; }

        public virtual ICollection<Show> Shows { get; set; }
    }
}
