﻿using System;
namespace MFP.Models
{
    public class SelectItem
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public SelectItem(string ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
        }

        public SelectItem()
        {
            ID = string.Empty;
            Name = string.Empty;
        }
    }
}
