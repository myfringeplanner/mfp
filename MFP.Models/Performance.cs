﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MFP.Models
{
    public class Performance
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ShowID { get; set; }

        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public decimal Price { get; set; }
        public decimal Concession { get; set; }
        public decimal ConcessionFamily { get; set; }
        public decimal ConcessionAdditional { get; set; }

        public virtual Show Show { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
