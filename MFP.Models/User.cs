﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MFP.Models
{
    public class User
    {
        [Key]
        public Guid ID { get; set; }
        [Required(ErrorMessage = "First Name is required")] [MinLength(2, ErrorMessage = "First Name must be at least 2 characters long")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")] [MinLength(2, ErrorMessage = "Last Name must be at least 2 characters long")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Email is required")] [EmailAddress(ErrorMessage = "Email is invalid")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password is required")] [MinLength(8, ErrorMessage = "Password must be at least 8 characters long")]
        public string Password { get; set; }
        public DateTime Created { get; set; }

        public virtual ICollection<Favourite> Favourites { get; set; }
        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
