﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace MFP.Models
{
    public static class EnumHelper
    {
        public static IEnumerable<EnumItem> GetEnumListItems<T>()
            where T : Enum
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Select(i => new EnumItem
            {
                Description = i.GetDescription() ?? i.ToString(),
                Value = i.ToString()
            });
        }

        //DO NOT USE - genre enum will be out of order/at risk of new categories or naming
        //public static List<SelectItem> GetEnumSelectItems<T>()
        //    where T : Enum
        //{
        //    return Enum.GetValues(typeof(T)).Cast<T>().Select(i => new SelectItem
        //    {
        //        ID = i.ToString(),
        //        Name = i.GetDescription() ?? i.ToString()
        //    }).ToList();
        //}

        public static string GetDescription(this Enum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());

            if (fieldInfo == null)
                return null;

            var attribute = (DescriptionAttribute)fieldInfo.GetCustomAttribute(typeof(DescriptionAttribute));

            return attribute?.Description ?? value.ToString();
        }

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static T GetEnumFromDescription<T>(string value)
            where T : Enum
        {
            var list = GetEnumListItems<T>();
            var enumValue = list.FirstOrDefault(i => i.Description == value)?.Value ?? "Other";

            return ParseEnum<T>(enumValue);
        }
    }
}
